use serde::{Deserialize, Serialize};
use surrealdb::engine::remote::ws::{Client, Ws};
use surrealdb::opt::auth::Root;
use surrealdb::sql::Thing;
use surrealdb::Surreal;

static DB: Surreal<Client> = Surreal::init();

#[derive(Debug, Serialize)]
pub struct Task<'a> {
    id: &'a str,
    content: &'a str,
    is_done: bool,
}

#[derive(Debug, Deserialize)]
pub struct Record {
    #[allow(dead_code)]
    pub id: Thing,
    #[allow(dead_code)]
    pub content: String,
    #[allow(dead_code)]
    pub is_done: bool,
}

pub async fn add_task(i: &str, c: &str, d: bool) -> surrealdb::Result<()> {
    // Connect to the database
    DB.connect::<Ws>("127.0.0.1:8000").await?;

    // Signin as a namespace, database, or root user
    DB.signin(Root {
        username: "root",
        password: "root",
    })
    .await?;

    // Select a specific namespace / database
    DB.use_ns("test").use_db("test").await?;

    // Create a new task
    let _ : Record = DB
        .create("task")
        .content(Task {
            id: i,
            content: c,
            is_done: d,
        })
        .await?;
    Ok(())
}

pub async fn toggle_done(i: &str, c: &str, d: bool) -> surrealdb::Result<()> {
    // Connect to the database
    DB.connect::<Ws>("127.0.0.1:8000").await?;

    // Signin as a namespace, database, or root user
    DB.signin(Root {
        username: "root",
        password: "root",
    })
    .await?;

    // Select a specific namespace / database
    DB.use_ns("test").use_db("test").await?;

    // Update task
    let _ : Record = DB
        .update(("task", i))
        .merge(Task {
            id: i,
            content: c,
            is_done: d,
        })
        .await?;

    Ok(())
}

pub async fn delete_tasks() -> surrealdb::Result<()> {
    // Connect to the database
    DB.connect::<Ws>("127.0.0.1:8000").await?;

    // Signin as a namespace, database, or root user
    DB.signin(Root {
        username: "root",
        password: "root",
    })
    .await?;

    // Select a specific namespace / database
    DB.use_ns("test").use_db("test").await?;

    // Update task
    let _ : Vec<Record> = DB.delete("task").await?;

    Ok(())
}

pub async fn import_tasks() -> surrealdb::Result<Vec<(String, bool)>> {
    // Connect to the database
    DB.connect::<Ws>("127.0.0.1:8000").await?;

    // Sign in as a namespace, database, or root user
    DB.signin(Root {
        username: "root",
        password: "root",
    })
    .await?;

    // Select a specific namespace / database
    DB.use_ns("test").use_db("test").await?;

    // Get tasks
    let tasks: Vec<Record> = DB.select("task").await?;

    // Create a 2D matrix to store content and is_done
    let task_matrix: Vec<(String, bool)> = tasks
        .iter()
        .map(|record| (record.content.clone(), record.is_done))
        .collect();

    Ok(task_matrix)
}
