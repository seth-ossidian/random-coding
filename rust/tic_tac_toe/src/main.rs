use std::io;
use std::io::Write;
use std::process::Command;

fn read_i8() -> i8 {
    print!("=> ");
    io::stdout().flush().unwrap();

    let mut ui = String::new();
    ui.clear();
    io::stdin().read_line(&mut ui).unwrap();
    ui = ui.trim().to_string();

    match ui.trim().parse::<i8>(){
        Ok(index) => {
            return index;
        }
        Err(_) => {
            return 0;
        }
    }
}

fn draw_board(b: &mut [[char; 3]; 3], c: &mut i8) {
    //clearing the terminal
    Command::new("clear").status().unwrap();

    //prinitng the board
    println!(" {}", *c);
    for row in *b {
        println!(" {} - {} - {}", row[0], row[1], row[2]);
    }

    //read key
    let mut index: i8 = 0;
    let x_o: [char; 2] = ['X', 'O'];
    let cnt = *c % 2;

    //deal with the input
    while index <= 0 || index > 9 || is_taken(index, *b) {
        index = read_i8();
    }

    match index {
        1 => { b[0][0] = x_o[cnt as usize]; *c+= 1; }
        2 => { b[0][1] = x_o[cnt as usize]; *c+= 1; }
        3 => { b[0][2] = x_o[cnt as usize]; *c+= 1; }
        4 => { b[1][0] = x_o[cnt as usize]; *c+= 1; }
        5 => { b[1][1] = x_o[cnt as usize]; *c+= 1; }
        6 => { b[1][2] = x_o[cnt as usize]; *c+= 1; }
        7 => { b[2][0] = x_o[cnt as usize]; *c+= 1; }
        8 => { b[2][1] = x_o[cnt as usize]; *c+= 1; }
        9 => { b[2][2] = x_o[cnt as usize]; *c+= 1; }
        _ => println!("error"),
    }

    //funcions
    fn is_taken(i: i8, b: [[char; 3]; 3]) -> bool {
        match i {
            1 => if b[0][0] == 'X' || b[0][0] == 'O' { return true; } else { return false; }
            2 => if b[0][1] == 'X' || b[0][1] == 'O' { return true; } else { return false; }
            3 => if b[0][2] == 'X' || b[0][2] == 'O' { return true; } else { return false; }
            4 => if b[1][0] == 'X' || b[1][0] == 'O' { return true; } else { return false; }
            5 => if b[1][1] == 'X' || b[1][1] == 'O' { return true; } else { return false; }
            6 => if b[1][2] == 'X' || b[1][2] == 'O' { return true; } else { return false; }
            7 => if b[2][0] == 'X' || b[2][0] == 'O' { return true; } else { return false; }
            8 => if b[2][1] == 'X' || b[2][1] == 'O' { return true; } else { return false; }
            9 => if b[2][2] == 'X' || b[2][2] == 'O' { return true; } else { return false; }
            _ => false
        }
    }
}

fn is_over(b: [[char; 3]; 3], c: i8) -> i8 {
    if b[0][0] == b[0][1] && b[0][0] == b[0][2] { return 1; }
    else if b[1][0] == b[1][1] && b[1][0] == b[1][2] { return 1; }
    else if b[2][0] == b[2][1] && b[2][0] == b[2][2] { return 1; }
    else if b[0][0] == b[1][0] && b[0][0] == b[2][0] { return 1; }
    else if b[0][1] == b[1][1] && b[0][1] == b[2][1] { return 1; }
    else if b[0][2] == b[1][2] && b[0][2] == b[2][2] { return 1; }
    else if b[0][0] == b[1][1] && b[0][0] == b[2][2] { return 1; }
    else if b[0][2] == b[1][1] && b[0][2] == b[2][0] { return 1; }
    else if c == 9 { return 2; }
    else { return 0; }
}

fn main() {
    //vars
    let mut board: [[char; 3]; 3] = [['1', '2', '3'], ['4', '5', '6'], ['7', '8', '9']];
    let mut count: i8 = 0;
    
    while is_over(board, count) == 0 { draw_board(&mut board, &mut count) }
    
    //clearing the terminal
    Command::new("clear").status().unwrap();

    //prinitng the board
    println!(" {}", count);
    for row in board {
        println!(" {} - {} - {}", row[0], row[1], row[2]);
    }

    if is_over(board, count) == 1 {
        if count % 2 == 0 { println!("O WINS!!!") }
        else { println!("X WINS!!!") }
    } else { println!("even :/") }

}
