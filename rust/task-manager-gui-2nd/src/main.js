const { invoke } = window.__TAURI__.tauri;

// Get elements
const input = document.querySelector("#user-input");
const list = document.querySelector("#tasks-list");
const form = document.querySelector("#form");

invoke("start_surreal", {});
invoke("import_surreal", {});

// Function to add a new task
async function addItem() {
  const userInput = input.value.trim();
  if (userInput !== '') {
    let _ = await invoke("surreal_add_task", {
      i: (list.getElementsByTagName('li').length + 1).toString(),
      c: userInput,
      d: false
    })
    .then(() => {
      const newItem = document.createElement('li');
      newItem.addEventListener('click', () => {
        newItem.classList.toggle('completed');
        let itemsArray = Array.from(document.querySelectorAll('li'));
        let _ =  invoke("surreal_update_task", {
          i: (itemsArray.indexOf(newItem) + 1).toString(),
          c: userInput,
          d: newItem.className === 'completed'
        });
      });

      newItem.textContent = userInput;
      input.value = '';
      list.appendChild(newItem);
    });
  }
}

function exportSurreal() {
  invoke("export_surreal", {});
}  

async function importTasks() {
  let tasks = await invoke("import_tasks", {});
  for (let task of tasks) {
    const newItem = document.createElement('li');
    newItem.addEventListener('click', () => {
      newItem.classList.toggle('completed');

      let itemsArray = Array.from(document.querySelectorAll('li'));
      invoke("surreal_update_task", {
        i: (itemsArray.indexOf(newItem) + 1).toString(),
        c: task[0],
        d: newItem.className === 'completed'
      })
    })
    newItem.textContent = task[0];
    if (task[1]) { newItem.classList.toggle('completed'); }
    list.appendChild(newItem);
  }
}  

async function killSurreal() {
  invoke("kill_surreal", {});
}

async function clearSurreal() {
  invoke("delete_tasks", {});
  list.innerHTML = '';
}

window.addEventListener("DOMContentLoaded", () => {
  document.querySelector("#form").addEventListener("submit", (e) => {
    e.preventDefault();
    addItem();
  });

  document.querySelector("#export").addEventListener("click", (e) => {
    e.preventDefault();
    exportSurreal();
  });

  document.querySelector("#import").addEventListener("click", (e) => {
    e.preventDefault();
    importTasks();
  });

  document.querySelector("#clear").addEventListener("click", (e) => {
    e.preventDefault();
    clearSurreal();
  });

  document.querySelector("#quite").addEventListener("click", (e) => {
    e.preventDefault();
    killSurreal();
  });
});
