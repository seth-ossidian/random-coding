// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

extern crate surrealdb_lib;
use std::process::Command;

#[tauri::command]
async fn surreal_add_task(i: &str, c: &str, d: bool) -> Result<(), ()> {
    use surrealdb_lib::add_task;
    let _ = add_task(i, c, d).await;
    Ok(())
}

#[tauri::command]
async fn surreal_update_task(i: &str, c: &str, d: bool) -> Result<(), ()> {
    use surrealdb_lib::toggle_done;
    let _ = toggle_done(i, c, d).await;
    Ok(())
}

async fn import_tasks_raw() -> Vec<(String, bool)> {
    use surrealdb_lib::import_tasks;

    let sm = import_tasks().await;
    let mut vec: Vec<(String, bool)> = vec![];

    match sm {
        Ok(m) => {
            for s in m {
                vec.push((s.0, s.1));
            }
        }
        Err(err) => {
            eprintln!("{:?}", err);
        }
    }

    vec
}

#[tauri::command]
async fn import_tasks() -> Vec<(String, bool)> {
    let tasks = import_tasks_raw().await;
    tasks
}

#[tauri::command]
async fn delete_tasks() -> Result<(), ()> {
    use surrealdb_lib::delete_tasks;
    let _ = delete_tasks().await;
    Ok(())
}

#[tauri::command]
async fn kill_surreal() {
    let _ = Command::new("pkill")
        .arg("surreal")
        .status()
        .expect("Failed to execute command");

    std::process::exit(0);
}

#[tauri::command]
async fn start_surreal() {
    let _ = Command::new("surreal")
        .arg("start")
        .arg("--user")
        .arg("root")
        .arg("--pass")
        .arg("root")
        .arg("memory")
        .status()
        .expect("Failed to execute command");
}

#[tauri::command]
async fn import_surreal() {
    let _ = Command::new("surreal")
        .arg("import")
        .arg("--conn")
        .arg("http://127.0.0.1:8000")
        .arg("--user")
        .arg("root")
        .arg("--pass")
        .arg("root")
        .arg("--ns")
        .arg("test")
        .arg("--db")
        .arg("test")
        .arg("database.surql")
        .status()
        .expect("Failed to execute command");
}

#[tauri::command]
async fn export_surreal() {
    let _ = Command::new("surreal")
        .arg("export")
        .arg("--conn")
        .arg("http://127.0.0.1:8000")
        .arg("--user")
        .arg("root")
        .arg("--pass")
        .arg("root")
        .arg("--ns")
        .arg("test")
        .arg("--db")
        .arg("test")
        .arg("database.surql")
        .status()
        .expect("Failed to execute command");
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            start_surreal,
            kill_surreal,
            export_surreal,
            import_surreal,
            import_tasks,
            delete_tasks,
            surreal_update_task,
            surreal_add_task
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
