package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	// Input and output file paths
	inputFile := "skull.txt"
	outputFile := "skull.tex"

	// Read the input .txt file
	inputContent, err := readTextFile(inputFile)
	if err != nil {
		fmt.Println("Error reading the input file:", err)
		return
	}

	// Convert ASCII art to LaTeX
	latexContent := convertToLatex(inputContent)

	// Write the LaTeX content to the output .tex file
	err = writeTexFile(outputFile, latexContent)
	if err != nil {
		fmt.Println("Error writing the LaTeX file:", err)
		return
	}

	fmt.Println("Conversion to LaTeX successful.")
}

func readTextFile(filepath string) (string, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var content string
	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadString('\n')
		if err != nil && err != io.EOF {
			return "", err
		}
		content += line
		if err == io.EOF {
			break
		}
	}

	return content, nil
}

func convertToLatex(input string) string {
	// Perform necessary replacements to convert ASCII art to LaTeX
	// (You can modify this based on the specific ASCII art you have in your .txt file)
	latex := strings.ReplaceAll(input, "_", "\\_")
	latex = strings.ReplaceAll(latex, "|", "\\textbar{}")

	return latex
}

func writeTexFile(filepath, content string) error {
	file, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString(content)
	return err
}
