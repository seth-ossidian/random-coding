package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func HandleError(err error) {
    if err != nil {
        log.Fatal(err)
    }
}

func main() {
    types, err:= ioutil.ReadFile("types.txt")
    HandleError(err)

    sizes, err:= ioutil.ReadFile("sizes.txt")
    HandleError(err)

    names, err:= ioutil.ReadFile("names.txt")
    HandleError(err)

    type_lines:= strings.Split(string(types), "\n")
    sizes_lines:= strings.Split(string(sizes), "\n")
    names_lines:= strings.Split(string(names), "\n")

    for i := range names_lines {
        names_lines[i] = strings.Replace(names_lines[i], "└─", "", -1)
        names_lines[i] = strings.Replace(names_lines[i], "├─", "", -1)
    }        

    for i, line := range type_lines {
        if line == "part"{
            fmt.Printf("%v) %v, %v\n",i, names_lines[i] ,sizes_lines[i])
        }
    }
}
