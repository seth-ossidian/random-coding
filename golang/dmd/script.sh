#!/bin/bash

# Scanning
lsblk | sed '1,8d' | sudo awk '{print $6}' > types.txt
lsblk | sed '1,8d' | sudo awk '{print $4}' > sizes.txt
lsblk | sed '1,8d' | sudo awk '{print $1}' > names.txt

# Drive to mount
dirtomount=$(./dmd | dmenu -p "mount:" -fn "CaskaydiaCoveNerdFont Mono:size=9:Bold" -nb "#282828" -nf "#ebdbb2" -sb "#d79921" -sf "#282828"| cut -c4-7)

# Path to mount to
[[ -n $dirtomount ]] && mountpath=$(ls /media | dmenu -p "mount:" -fn "CaskaydiaCoveNerdFont Mono:size=9:Bold" -nb "#282828" -nf "#ebdbb2" -sb "#d79921" -sf "#282828")

# Mounting
[[ -n $mountpath ]] && echo mounting $dirtomount to $mountpath...
[[ -n $mountpath ]] && sudo mount -o umask=0 /dev/$dirtomount /media/$mountpath
